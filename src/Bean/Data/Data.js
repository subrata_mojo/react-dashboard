/*
  @author: Subrata Banerjee
  @created_date: 10/12/2018 
*/
import * as R from 'ramda';
export class DataBean {

  constructor() {

    /* this.data will store the dashboard
    data point loaded from the CSV file */
    
    this.data = [];
  }

  /* setData method will set the loaded
  JSON data into data */

  setData = (value) => {
    if (!R.isEmpty(value)) {
      this.data = R.remove(0, 1 , value);
      console.log(this.data);
    }
  }

  /* getData will return
  the data stored in data */

  getData = () => {
    return this.data;
  }
}