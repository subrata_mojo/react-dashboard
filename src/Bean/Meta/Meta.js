/*
  @author: Subrata Banerjee
  @created_date: 10/12/2018 
*/
import * as R from 'ramda';
export class MetaBean {

  constructor() {

    /* this.meta will store the dashboard
    meta point loaded from the CSV file */
    
    this.meta = [];
  }

  /* setMeta method will set the loaded
  JSON data into meta */

  setMeta = (value) => {
    if (!R.isEmpty(value)) {
      this.meta = value[0];
      console.log(this.meta);
    }
  }

  /* getMeta will return
  the data stored in meta */

  getMeta = () => {
    return this.meta;
  }
}