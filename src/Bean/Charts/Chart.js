/*
  @author: Subrata Banerjee
  @created_date: 10/12/2018 
*/
import * as R from 'ramda';
export class ChartBean {

  constructor() {

    /* this.chart will store the dashboard
    chart details with configurations */
    
    this.chart = [
        {
            type: 'LINE_CHART',
            name: 'Line Chart',
            icon: 'show_chart'
        }, 
        {
            type: 'PIE_CHART',
            name: 'Pie Chart',
            icon: 'pie_chart'
        },
        {
            type: 'BUBBLE_CHART',
            name: 'Bubble Chart',
            icon: 'bubble_chart'
        }
    ];
  }

  /* getChart will return
  the chart stored in this.chart */

  getChart = () => {
    return this.chart;
  }
}