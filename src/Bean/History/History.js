/*
  @author: Subrata Banerjee
  @created_date: 10/12/2018 
*/
import * as R from 'ramda';
export class HistoryBean {

  constructor() {

    /* this.history will store the dashboard
    charts history as we start using it. */
    
    this.history = [];
  }

  /* setHistory will push each 
  of the history object to this.history array */

  setHistory = (type, data) => {
    if (!R.isEmpty(data)) {
      this.history.push({
        type: type,
        data:data
      });
    }
  }

  /* getHistory will return the
  history array */

  getHistory = () => {
    return this.history;
  }
}