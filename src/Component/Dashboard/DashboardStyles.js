export const styles = {
    dashboardContainer: {
        width: 'inherit',
        height: 'inherit',
        display: 'flex',
        flexDirection: 'column'
    },
    appTopNavbar: {
        width: 'calc(100% - 20px)',
        height: 50,
        border: '1px solid #e0e0e0',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: '0 10px'
    },
    appTopNavbar__h4: {
        fontWeight: 400
    },
    appAction__Button: {
        minWidth: 'auto',
        minHeight: 'auto',
        padding: 8,
        borderRadius: 100
    },
    appBody: {
        width: 'calc(100% - 20px)',
        padding: '0 10px',
        height: 'calc(100% - 50px)',
        display: 'flex',
        'flex-direction': 'row'
    },
    chartListNavVertical: {
        width: 'calc(150px - 10px)',
        height: 'calc(100% - 10px)',
        display: 'flex',
        'flex-direction': 'column',
        'border-right': '1px solid #e0e0e0',
        padding: 5
    },
    chartListNavHorizontal: {
        width: 'calc(100% - 20px)',
        height: 100,
        display: 'flex',
        flexDirection: 'row',
    },
    chartDroppableContainer: {
        width: '100%'
    },
    chartDroppableHeader: {
        width: 'calc(100% - 20px)',
        height: 100,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        margin: '10px',
        'box-shadow': '0 0 !important',
        border: '1px dashed #e0e0e0',
        'background-color': '#f9f9f9 !important'
    },
    chartBody: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
    },
    lineChartContainer: {
        width: 'calc(100% - 20px)',
        margin: '10px'
    }
}