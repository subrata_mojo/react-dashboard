import React from 'react';
import { Button, Paper, Dialog, Slide, DialogTitle, DialogContent, DialogActions, List, ListItem, ListItemText } from '@material-ui/core';
import HistoryRounded from '@material-ui/icons/HistoryRounded';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import ChartList from '../ChartList';
import ChartData from '../ChartData';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './DashboardStyles';
import { Droppable } from 'react-drag-and-drop';
import {
    BrowserView,
    MobileView,
    isBrowser,
    isMobile
  } from "react-device-detect";
  import * as R from 'ramda';
  import { Chart } from 'react-google-charts';

  function Transition(props) {
    return <Slide direction="up" {...props} />;
  }
class Dashboard extends React.Component {

    constructor() {
        super();
        
        this.state = {
            currentChart: null,
            showModal: false,
            showHistoryModal: false,
            LINE_CHART: {
                XAxisType: '',
                XAxisName: '',
                YAxisType: [],
                YAxisName: '',
                XAxisData: [],
                YAxisData: []
            },
            showLineChart: false,
            LINE_CHART_DATA: [],
            LINE_CHART_OPTIONS: {
                hAxis: {
                    title: '',
                },
                vAxis: {
                    title: '',
                },
            },
            PIE_CHART: {
                XAxisType: '',
                YAxisType: '',
                title: '',
            },
            showPieChart: false,
            PIE_CHART_DATA: [],
            PIE_CHART_OPTIONS: {
                hAxis: {
                    title: '',
                },
                vAxis: {
                    title: '',
                },
            },
            BUBBLE_CHART: {
                XAxisType: '',
                YAxisType: [],
                title: '',
            },
            showBubbleChart: false,
            BUBBLE_CHART_DATA: [],
            BUBBLE_CHART_OPTIONS: {
                hAxis: {
                    title: '',
                },
                vAxis: {
                    title: '',
                },
            }
        }

        this.handleModalDone = this.handleModalDone.bind(this);
        this.handleHistoryModalClose = this.handleHistoryModalClose.bind(this);

        console.log(isBrowser);
        console.log(isMobile);
    }

    onDrop = (data) => {
        this.setState({
            currentChart: data
        }, this.handleClose(true));
    }

    handleClose = (state) => {
        this.setState({
            ...state,
            showModal: state
        });
    }

    handleToggle = () => {
        this.props.toggle(R.F());
    }

    handleHistoryToggle = (state) => {
        this.setState({
            ...state,
            showHistoryModal: true
        });
    }

    handleHistoryModalClose = (state) => {
        this.setState(prevState => ({
            ...prevState,
            showHistoryModal: state
        }));
        this.handleModalDone();
    }

    handleModalDone = (index) => {
        let data = [];
        let XTypeData = [];
        let YTypeData = [];

        if (this.state.currentChart !== null) {
            if (this.state.currentChart.charts === 'LINE_CHART') {
                let historyDataIndex = R.findLastIndex(R.propEq('type', this.state.currentChart.charts))(this.props.history.getHistory());
                if (index !== undefined) {
                    historyDataIndex = index;
                }
                if (historyDataIndex !== -1) {
                    const chartDataHistory = this.props.history.getHistory()[historyDataIndex].data;
                    data.push([chartDataHistory.XAxisName]);
                    for (let i = 0; i < chartDataHistory.YAxisType.length; i++) {
                        data[0].push(chartDataHistory.YAxisType[i])
                    }
                    const XTypeIndex = R.indexOf(chartDataHistory.XAxisType, this.props.metaBean.getMeta());
                    this.props.dataBean.getData().forEach((e, i) => {
                        if (e[XTypeIndex] !== undefined) {
                            XTypeData.push(e[XTypeIndex]);
                        }
                    });
    
                    chartDataHistory.YAxisType.forEach((f, j) => {
                        const YTypeIndex = R.indexOf(f, this.props.metaBean.getMeta());
                        this.props.dataBean.getData().forEach((e, i) => {
                            if (e[YTypeIndex] !== undefined) {
                                if (YTypeData[i] === undefined) {
                                    YTypeData.push([e[YTypeIndex]]);
                                } else {
                                    YTypeData[i].push(e[YTypeIndex]);
                                }
                            }
                        });
                    });
    
                    console.log(YTypeData);
    
                    for (let i = 1 ; i < XTypeData.length; i++) {
                        data.push([i]);
                    }
    
                    for (let i = 1 ; i < YTypeData.length; i++) {
                        for (let j = 0; j < YTypeData[i].length; j++) {
                            data[i].push(parseFloat(YTypeData[i][j]));
                        }
                    }
                    console.log('LC', data);
                    this.setState(prevState => ({
                        ...prevState,
                        showLineChart: true,
                        showPieChart: false,
                        showBubbleChart: false,
                        LINE_CHART_OPTIONS: {
                            hAxis: {
                                title: chartDataHistory.XAxisName
                            },
                            vAxis: {
                                title: chartDataHistory.YAxisName,
                            }
                        },
                        LINE_CHART_DATA: data,
                        showModal: false
                    }))
                } else {
                    console.log('Error in chart data. Try again')
                }
            } else if (this.state.currentChart.charts === 'PIE_CHART') {
                let historyDataIndex = R.findLastIndex(R.propEq('type', this.state.currentChart.charts))(this.props.history.getHistory());
                if (index !== undefined) {
                    historyDataIndex = index;
                }
                if (historyDataIndex !== -1) {
                    const chartDataHistory = this.props.history.getHistory()[historyDataIndex].data;
                    data.push([chartDataHistory.XAxisType, chartDataHistory.YAxisType]);
                    const XTypeIndex = R.indexOf(chartDataHistory.XAxisType, this.props.metaBean.getMeta());
                    const YTypeIndex = R.indexOf(chartDataHistory.YAxisType, this.props.metaBean.getMeta());
                    if (XTypeIndex !== -1 && YTypeIndex !== -1) {
                        this.props.dataBean.getData().forEach((e, i) => {
                            if (e[XTypeIndex] !== undefined) {
                                XTypeData.push(e[XTypeIndex]);
                            }
                        });
                        this.props.dataBean.getData().forEach((e, i) => {
                            if (e[YTypeIndex] !== undefined) {
                                YTypeData.push(e[YTypeIndex]);
                            }
                        });
    
        
                        for (let i = 0 ; i < XTypeData.length; i++) {
                            data.push([XTypeData[i], parseFloat(YTypeData[i])]);
                        }
                        this.setState({
                            showLineChart: false,
                            showPieChart: true,
                            showBubbleChart: false,
                            PIE_CHART_OPTIONS: {
                                title: chartDataHistory.title
                            },
                            PIE_CHART_DATA: data,
                            showModal: false
                        })
                    } else {
                        console.log('Error in chart data. Try again')    
                    }
                } else {
                    console.log('Error in chart data. Try again')
                }
            } else if (this.state.currentChart.charts === 'BUBBLE_CHART') {
                console.log(this.props.history.getHistory());
                console.log( R.findLastIndex(R.propEq('type', this.state.currentChart.charts))(this.props.history.getHistory()));
                let historyDataIndex = R.findLastIndex(R.propEq('type', this.state.currentChart.charts))(this.props.history.getHistory());
                if (index !== undefined) {
                    historyDataIndex = index;
                }
                if (historyDataIndex !== -1) {
                    const chartDataHistory = this.props.history.getHistory()[historyDataIndex].data;
                    data.push([chartDataHistory.XAxisType]);
                    for (let i = 0; i < chartDataHistory.YAxisType.length; i++) {
                        data[0].push(chartDataHistory.YAxisType[i])
                    }
                    const XTypeIndex = R.indexOf(chartDataHistory.XAxisType, this.props.metaBean.getMeta());
                    this.props.dataBean.getData().forEach((e, i) => {
                        if (e[XTypeIndex] !== undefined) {
                            XTypeData.push(e[XTypeIndex]);
                        }
                    });
    
                    chartDataHistory.YAxisType.forEach((f, j) => {
                        const YTypeIndex = R.indexOf(f, this.props.metaBean.getMeta());
                        this.props.dataBean.getData().forEach((e, i) => {
                            if (e[YTypeIndex] !== undefined) {
                                if (YTypeData[i] === undefined) {
                                    YTypeData.push([e[YTypeIndex]]);
                                } else {
                                    YTypeData[i].push(e[YTypeIndex]);
                                }
                            }
                        });
                    });
    
                    console.log(YTypeData);
    
                    for (let i = 1 ; i < XTypeData.length; i++) {
                        data.push([XTypeData[i]]);
                    }
                    console.log(data);
    
                    for (let i = 1 ; i < YTypeData.length; i++) {
                        for (let j = 0; j < YTypeData[i].length; j++) {
                            data[i].push(parseFloat(YTypeData[i][j]));
                        }
                    }
    
                    console.log(data);
                        this.setState({
                            showLineChart: false,
                            showPieChart: false,
                            showBubbleChart: true,
                            BUBBLE_CHART_OPTIONS: {
                                title: chartDataHistory.title
                            },
                            BUBBLE_CHART_DATA: data,
                            showModal: false
                        })
                    } else {
                        console.log('Error in chart data. Try again')    
                    }
                } 
            }
    }

    showHistory = () => {
        console.log(this.props.history.getHistory());

        const historyDOM = this.props.history.getHistory().map((e, i) => {
            return (
                <ListItem button key={i} id={i} onClick={(event) => this.handleHistoryPop(event, e, i)}>
                    <ListItemText primary={e.type} />
                </ListItem>
            )
        });

        return historyDOM;
    }

    handleHistoryPop = (e, d, i) => {
        e.persist();
        let chart = this.state.currentChart;
        chart.charts = d.type
        this.setState(prevState => ({
            ...this.state,
            currentChart: chart,
            showHistoryModal: false
        }));
        this.handleModalDone(i);
    }
    render() {
        const { classes } = this.props;
        return (
            <React.Fragment>
                <div className={classes.dashboardContainer}>
                    <div className={classes.appTopNavbar}>
                        <h4 className={classes.appTopNavbar__h4}>Dashboard</h4>
                        <div className={classes.appActionContainer}>
                        <Button className={classes.appAction__Button} aria-label="History" onClick={()  => this.handleHistoryToggle(true)}>
                            <HistoryRounded />
                        </Button>
                        <Button className={classes.appAction__Button} aria-label="Back" onClick={this.handleToggle}>
                            <KeyboardArrowLeft />
                        </Button>
                        </div>
                    </div>
                    <div className={classes.appBody}>
                        <BrowserView>
                            <div className={classes.chartListNavVertical}>
                                <ChartList/>
                            </div>
                        </BrowserView>
                        <div className={classes.chartBody}>
                            <MobileView>
                                <div className={classes.chartListNavHorizontal}>
                                    <ChartList/>
                                </div>
                            </MobileView>
                            <Paper className={classes.chartDroppableHeader}>
                                <Droppable
                                    types={['charts']}
                                    onDrop={this.onDrop.bind(this)}
                                    className={classes.chartDroppableContainer}>
                                    Drag and drop your chart here to start visulization.
                                </Droppable>
                            </Paper>
                            {
                                this.state.showLineChart &&
                                <Paper className={classes.lineChartContainer}>
                                <Chart
                                    width={'100%'}
                                    height={'400px'}
                                    chartType="LineChart"
                                    loader={<div>Loading Chart</div>}
                                    data={this.state.LINE_CHART_DATA}
                                    options={this.state.LINE_CHART_OPTIONS}
                                    rootProps={{ 'data-testid': '1' }}
                                />
                                </Paper>
                            }
                            {
                                this.state.showPieChart &&
                                <Paper className={classes.lineChartContainer}>
                                <Chart
                                    width={'100%'}
                                    height={'400px'}
                                    chartType="PieChart"
                                    loader={<div>Loading Chart</div>}
                                    data={this.state.PIE_CHART_DATA}
                                    options={this.state.PIE_CHART_OPTIONS}
                                    rootProps={{ 'data-testid': '1' }}
                                />
                                </Paper>
                            }
                            {
                                this.state.showBubbleChart &&
                                <Paper className={classes.lineChartContainer}>
                                <Chart
                                    width={'100%'}
                                    height={'400px'}
                                    chartType="BubbleChart"
                                    loader={<div>Loading Chart</div>}
                                    data={this.state.BUBBLE_CHART_DATA}
                                    options={this.state.BUBBLE_CHART_OPTIONS}
                                    rootProps={{ 'data-testid': '1' }}
                                />
                                </Paper>
                            }
                        </div>
                    </div>
                </div>
                <Dialog
                    open={this.state.showModal}
                    TransitionComponent={Transition}
                    keepMounted
                    onClose={() => this.handleClose(false)}
                    aria-labelledby="chartData"
                    >
                    <DialogTitle id="chartData">
                        {"Enter data for chart"}
                    </DialogTitle>
                    <DialogContent>
                        <ChartData chartType={this.state.currentChart} metaBean={this.props.metaBean}  dataBean={this.dataBean} history={this.props.history} modalClose={this.handleModalDone}/>
                    </DialogContent>
                    {/* <DialogActions>
                        <Button onClick={this.handleModalDone} color="primary">
                            Done
                        </Button>
                    </DialogActions> */}
                </Dialog>
                <Dialog
                    open={this.state.showHistoryModal}
                    TransitionComponent={Transition}
                    keepMounted
                    onClose={() => this.handleClose(false)}
                    aria-labelledby="chartHistoryData"
                    >
                    <DialogTitle id="chartHistoryData">
                        {"Chart History"}
                    </DialogTitle>
                    <DialogContent>
                        <List component="nav">
                            {this.showHistory()}
                        </List>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.handleHistoryModalClose(false)} color="primary">
                            Done
                        </Button>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
        )
    }
}

export default withStyles(styles)(Dashboard);