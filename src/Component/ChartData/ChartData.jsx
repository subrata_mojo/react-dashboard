import React from 'react';
import { TextField, FormControl, InputLabel, Select, MenuItem, Grid, Input, Chip, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './ChartDataStyles';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

class ChartData extends React.Component {

  constructor() {
    super();

    this.state = {
      lineChart: {
        XAxisType: '',
        XAxisName: '',
        YAxisType: [],
      },
      pieChart: {
        XAxisType: '',
        YAxisType: '',
        title: '',
      },
      bubbleChart: {
        XAxisType: '',
        YAxisType: [],
        title: ''
      }
    }
    this.handleChangeLineChart = this.handleChangeLineChart.bind(this);
  }

  handleChangeLineChart = (f, e) => {
    e.persist();
    let Property = this.state.lineChart
    Property[f] = e.target.value;
    this.setState({
      ...this.state,
      Property
    }, this.handleCallbackChart())
  }

  handleChangePieChart = (f, e) => {
    e.persist();
    let Property = this.state.pieChart
    Property[f] = e.target.value;
    this.setState({
      ...this.state,
      Property
    }, this.handleCallbackChart())
  }

  handleChangeBubbleChart = (f, e) => {
    e.persist();
    let Property = this.state.bubbleChart
    Property[f] = e.target.value;
    this.setState({
      ...this.state,
      Property
    }, this.handleCallbackChart())
  }

  handleCallbackChart = () => {};

  handleIsValidData = (v) => {
    const vKeys = Object.keys(v);
    let validationCount = 0;
    for (let i = 0; i < vKeys.length; i++) {
      if (typeof v[vKeys[i]] === 'string') {
        if (v[vKeys[i]] !== '') {
          validationCount++;
        }
      } else if (Array.isArray(v[vKeys[i]]) && v[vKeys[i]].length > 0) {
        validationCount++
      }
    }

    if (vKeys.length === validationCount) {
      return true;
    } else {
      return false;
    }
  }

  handleModalDone = () => {
    if (this.props.chartType.charts === 'LINE_CHART') {
      if (this.handleIsValidData(this.state.lineChart)) {
        this.props.history.setHistory('LINE_CHART', this.state.lineChart);
      }
    } else if (this.props.chartType.charts === 'PIE_CHART') {
      if (this.handleIsValidData(this.state.pieChart)) {
        this.props.history.setHistory('PIE_CHART', this.state.pieChart); 
      }
    } else if (this.props.chartType.charts === 'BUBBLE_CHART') {
      if (this.handleIsValidData(this.state.bubbleChart)) {
        this.props.history.setHistory('BUBBLE_CHART', this.state.bubbleChart); 
      }
    }
    this.props.modalClose();
  }

  getAxisDropdown = () => {
    const htmlDOM = this.props.metaBean.getMeta().map((e, i) => {
      return (
        <MenuItem key={i} value={e}>
          {e}
        </MenuItem>
      )
    });
    return htmlDOM;
  }

  showChartDataCollector = () => {
    const classes = {
      fieldFullWidth: {
        width: '100%'
      }
    }
    if (this.props.chartType && this.props.chartType.charts === 'LINE_CHART') {
      const htmlDOM = (
        <div>
          <form noValidate autoComplete="off">
            <Grid container spacing={16}>
              <Grid item xs={12} sm={12}>
                <FormControl style={classes.fieldFullWidth}> 
                  <InputLabel htmlFor="x-axis-type">Select column for X-Axis</InputLabel>
                  <Select
                    value={this.state.lineChart.XAxisType}
                    onChange={(e) => this.handleChangeLineChart('XAxisType', e)}
                    inputProps={{
                      name: 'x-axis-type',
                      id: 'x-axis-type',
                    }}
                    style={classes.fieldFullWidth}
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    {this.getAxisDropdown()}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={12}>
                <FormControl style={classes.fieldFullWidth}>
                  <InputLabel htmlFor="y-axis-type">Select column for Y-Axis</InputLabel>
                  <Select
                    multiple
                    value={this.state.lineChart.YAxisType}
                    onChange={(e) => this.handleChangeLineChart('YAxisType', e)}
                    input={<Input id="select-multiple-chip" />}
                    renderValue={selected => (
                      <div className={classes.chips}>
                        {selected.map(value => (
                          <Chip key={value} label={value} className={classes.chip} />
                        ))}
                      </div>
                    )}
                    MenuProps={MenuProps}
                  >
                    {this.props.metaBean.getMeta().map(name => (
                      <MenuItem key={name} value={name} >
                        {name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  id="x-axis-name"
                  label="X-Axis Name"
                  value={this.state.lineChart.XAxisName}
                  onChange={(e) => this.handleChangeLineChart('XAxisName', e)}
                  margin="normal"
                  style={classes.fieldFullWidth}
                />
              </Grid>
            </Grid>
          </form>
        </div>
      )
      return htmlDOM;
    } else if (this.props.chartType && this.props.chartType.charts === 'PIE_CHART') {
      const htmlDOM = (
        <div>
          <form noValidate autoComplete="off">
            <Grid container spacing={16}>
              <Grid item xs={12} sm={12}>
                <FormControl style={classes.fieldFullWidth}> 
                  <InputLabel htmlFor="x-axis-type">Select column for X-Axis</InputLabel>
                  <Select
                    value={this.state.pieChart.XAxisType}
                    onChange={(e) => this.handleChangePieChart('XAxisType', e)}
                    inputProps={{
                      name: 'x-axis-type',
                      id: 'x-axis-type',
                    }}
                    style={classes.fieldFullWidth}
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    {this.getAxisDropdown()}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={12}>
                <FormControl style={classes.fieldFullWidth}>
                  <InputLabel htmlFor="y-axis-type">Select column for Y-Axis</InputLabel>
                  <Select
                    value={this.state.pieChart.YAxisType}
                    onChange={(e) => this.handleChangePieChart('YAxisType', e)}
                    inputProps={{
                      name: 'y-axis-type',
                      id: 'y-axis-type',
                    }}
                    style={classes.fieldFullWidth}
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    {this.getAxisDropdown()}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  id="title"
                  label="title"
                  value={this.state.pieChart.title}
                  onChange={(e) => this.handleChangePieChart('title', e)}
                  margin="normal"
                  style={classes.fieldFullWidth}
                />
              </Grid>
            </Grid>
          </form>
        </div>
      )

      return htmlDOM;
    } else if (this.props.chartType && this.props.chartType.charts === 'BUBBLE_CHART') {
      const htmlDOM = (
        <div>
          <form noValidate autoComplete="off">
            <Grid container spacing={16}>
              <Grid item xs={12} sm={12}>
                <FormControl style={classes.fieldFullWidth}> 
                  <InputLabel htmlFor="x-axis-type">Select column for X-Axis</InputLabel>
                  <Select
                    value={this.state.bubbleChart.XAxisType}
                    onChange={(e) => this.handleChangeBubbleChart('XAxisType', e)}
                    inputProps={{
                      name: 'x-axis-type',
                      id: 'x-axis-type',
                    }}
                    style={classes.fieldFullWidth}
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    {this.getAxisDropdown()}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={12}>
                <FormControl style={classes.fieldFullWidth}>
                  <InputLabel htmlFor="y-axis-type">Select column for Y-Axis</InputLabel>
                  <Select
                    multiple
                    value={this.state.bubbleChart.YAxisType}
                    onChange={(e) => this.handleChangeBubbleChart('YAxisType', e)}
                    input={<Input id="select-multiple-chip" />}
                    renderValue={selected => (
                      <div className={classes.chips}>
                        {selected.map(value => (
                          <Chip key={value} label={value} className={classes.chip} />
                        ))}
                      </div>
                    )}
                    MenuProps={MenuProps}
                  >
                    {this.props.metaBean.getMeta().map(name => (
                      <MenuItem key={name} value={name} >
                        {name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  id="title"
                  label="title"
                  value={this.state.bubbleChart.title}
                  onChange={(e) => this.handleChangeBubbleChart('title', e)}
                  margin="normal"
                  style={classes.fieldFullWidth}
                />
              </Grid>
            </Grid>
          </form>
        </div>
      )

      return htmlDOM;
    }
  }

  render() {
    return (
      <React.Fragment>
        {this.showChartDataCollector()}
        
        <Button variant="contained" color="primary" onClick={this.handleModalDone}>
          Done
        </Button>
      </React.Fragment>
    )
  }
}

export default withStyles(styles)(ChartData);