import React from 'react';
import Card from '@material-ui/core/Card';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './FileUploadStyles';
import Dropzone from 'react-dropzone';
import { CloudUploadRounded } from '@material-ui/icons';
import csv from 'csv';
import * as R from 'ramda';
class FileUpload extends React.Component {

  onDrop = (acceptedFiles) => {
    console.log(acceptedFiles);
    const Reader = new FileReader();
    Reader.onload = () => {
      csv.parse(Reader.result, (err, data) => {
        if (err) {
          console.log(err);
        } else {
          this.props.dataBean.setData(data);
          this.props.metaBean.setMeta(data);
          this.props.toggle(R.T());
        }
      });
    }

    Reader.readAsBinaryString(acceptedFiles[0]);
  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <h3 className={classes.fileUploadHeader}>Upload a CSV file to analyze</h3>
        <Card className={classes.fileUploadContainer}>
          <Dropzone onDrop={this.onDrop}>
            {({getRootProps, getInputProps, isDragActive}) => {
              return (
                <div
                  {...getRootProps()}
                  className={classes.fileDropZone}
                >
                  <input {...getInputProps()} />
                  {
                    isDragActive ?
                      <p className={classes.fileDropZone__p}>Drop files here...</p> :
                      <React.Fragment>
                        <CloudUploadRounded/>
                        <p className={classes.fileDropZone__p}>Try dropping some files here, or click to select files to upload.</p>
                      </React.Fragment>
                  }
                </div>
              )
            }}
          </Dropzone>
          <div className={classes.fileUploadLine}></div>
        </Card>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(FileUpload);