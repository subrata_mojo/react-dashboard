export const styles = {
  fileUploadHeader: {
    fontSize: '1rem',
    fontWeight: 300
  },
  fileUploadContainer: {
    width: 300,
    height: 150,
    position: 'relative'
  },
  fileDropZone: {
    width: 'calc(100% - 20px)',
    height: 'calc(100% - 20px)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
    padding: 10
  },
  fileDropZone__p: {
    margin: 0,
    textAlign: 'center',
    fontSize: '0.875rem',
    fontWeight: 300
  },
  fileUploadLine: {
    width: 'inherit',
    height: 4,
    backgroundColor: '#3f51b5',
    position: 'absolute',
    bottom: 0
  }
};