export const styles = {
  chartItemContainer: {
    display: 'flex',
    flexDirection: 'column'
  },
  chartItemPaper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '20px 0',
    margin: '5px 0'
  }
}