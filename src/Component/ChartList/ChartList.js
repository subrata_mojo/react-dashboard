import React from 'react';
import { ChartBean } from '../../Bean/Charts';
import Paper from '@material-ui/core/Paper';
import { ShowChartRounded, PieChartRounded, BubbleChartRounded } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './ChartListStyles';
import { Draggable } from 'react-drag-and-drop';

class ChartList extends React.PureComponent {

  constructor() {
    super();
    this.ChartBean = new ChartBean();
  }

  showChartList  = () => {
    const { classes } = this.props;
    console.log(this.ChartBean.getChart())
    const chartList = this.ChartBean.getChart().map((item, i) => {

      /* Load charts based on type, this charts are the Draggble component of drag-and-drop */
      if (item.type === 'LINE_CHART') {
        return (
          <Draggable type="charts" data={item.type} key={i} className={classes.chartItemContainer}>
            <Paper className={classes.chartItemPaper}>
              <ShowChartRounded className={classes.chartItemIcon}/>
              {item.name}
            </Paper>
          </Draggable>
        ); 
      } else if (item.type === 'PIE_CHART') {
        return (
          <Draggable type="charts" data={item.type} key={i} className={classes.chartItemContainer}>
            <Paper className={classes.chartItemPaper}>
              <PieChartRounded className={classes.chartItemIcon}/>
              {item.name}
            </Paper>
          </Draggable>
        );
      } else if (item.type === 'BUBBLE_CHART') {
        return (
          <Draggable type="charts" data={item.type} key={i} className={classes.chartItemContainer}>
            <Paper className={classes.chartItemPaper}>
              <BubbleChartRounded className={classes.chartItemIcon}/>
              {item.name}
            </Paper>
          </Draggable>
        );
      }
    });
    return chartList;
  }

  render() {
    return (
      <React.Fragment>
        {this.showChartList()}
      </React.Fragment>
    )
  }
}

export default withStyles(styles)(ChartList);