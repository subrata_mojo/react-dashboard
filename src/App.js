import React, { Component } from 'react';
import { DataBean } from './Bean/Data/Data';
import FileUpload from './Component/FileUpload/FileUpload';
import Dashboard from './Component/Dashboard/Dashboard';
import './App.css';
import { MetaBean } from './Bean/Meta/Meta';
import { HistoryBean } from './Bean/History/History'

class App extends Component {

  constructor(props) {
    super(props);
    this.dataBean = new DataBean();
    this.metaBean = new MetaBean();
    this.chartHistory = new HistoryBean();

    this.state = {
      showDashboard: false
    }
  }

  toggleComponent = (type) => {
    this.setState({
      showDashboard: type
    });
  }

  render() {
    const { showDashboard } = this.state;
    return (
      <React.Fragment>
        <div className="applicationContainer">
        {/* showDashboard flag will allow us to toggle between FileUpload and Dashboard component, once we upload the CSV */}
        {
          !showDashboard && <FileUpload dataBean={this.dataBean} metaBean={this.metaBean} toggle={this.toggleComponent}/>
        }
        {
          showDashboard && <Dashboard dataBean={this.dataBean} metaBean={this.metaBean} history={this.chartHistory} toggle={this.toggleComponent}></Dashboard>
        }
        </div>
      </React.Fragment>
    );
  }
}

export default App;
